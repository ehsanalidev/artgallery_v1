﻿using AutoMapper;
using Azure.Core;
using Bl.Abstractions;
using Bl.Abstractions.Dtos.Authenticate;
using BussinessLayer.DefaultValues;
using DataAccessLAyer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.WebSockets;

namespace ArtGallery_V1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        public AuthorizationController(IUserMangementServices mangementServices, IMapper mapper)
        {
            MangementServices = mangementServices;
            Mapper = mapper;
        }

        public IUserMangementServices MangementServices { get; }
        public IMapper Mapper { get; }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<AuthenticationResponse> Authenticate(AuthenticationRequest request)
        {
            var response = await MangementServices.AuthenticateAsync(request.Username, request.Password);
            return response;
        }
        [AllowAnonymous]
        [HttpPost("Register")]
        public async Task<AuthenticationResponse> Register([FromForm] RegisterRequest request)
        {
            var user = Mapper.Map<User>(request);
            user= await MangementServices.RegisterAsync(user, request.Password);
            await MangementServices.CreateUserAvatarAsync(user.Id, request.Avatar.OpenReadStream(), request.Avatar.FileName);
            var response = await MangementServices.AuthenticateAsync(user.Username, request.Password);
            return response;
        }
    }
   
}
