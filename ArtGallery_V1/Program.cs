using ArtGallery_V1.Utilities;
using Bl.Abstractions;
using BussinessLayer.Services;
using BussinessLayer.Utilietis;
using DataAccessLAyer;
using DataAccessLAyer.Models;
using Microsoft.EntityFrameworkCore;
using Serilog;
using AutoMapper;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddScoped<ISettings, Settings>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var appsetings= builder.Services.BuildServiceProvider().GetRequiredService<ISettings>();
builder.Services.AddDbContext<Artgallery2Context>(option => option.UseSqlServer(appsetings.DbConnectionString));
builder.Services.AddScoped<IUserMangementServices, UserManagementServices>();
builder.Services.AddScoped<ISeeder,  Seeder>();
builder.Services.AddScoped<IArtGalleryContext, Artgallery2Context>();
builder.Services.AddAutoMapper(typeof(Program));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
using(var scope= app.Services.CreateScope())
{
    try
    {
        Log.Information($"Migration started");
        var db= scope.ServiceProvider.GetRequiredService<Artgallery2Context>();
        Log.Information($"instance created");
        db.Database.Migrate();
        Log.Information($"Migration ended");


    }
    catch (Exception ex)
    {
        Log.Error($"MigrateDatabase()=> Faild, Exp:{ex}");


    }
}
using (var scope = app.Services.CreateScope())
{
    try
    {
        Log.Information($"Seed started");
        var seeder = scope.ServiceProvider.GetRequiredService<ISeeder>();
        Log.Information("Seed instance created");
        seeder.Seed();
        Log.Information($"Seed ended");
    }
    catch (Exception ex)
    {
        Log.Error($"SeederDatabase()=> Failed, Exp: {ex}");
    }
}
// commit test
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
