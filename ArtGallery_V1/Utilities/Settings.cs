﻿using BussinessLayer;
using BussinessLayer.Utilietis;

namespace ArtGallery_V1.Utilities

{
    public class Settings : ISettings
    {
        public string DefaultString = @"Server=.\SQLExpress;Database=artgallery31;Integrated Security=True;TrustServerCertificate=true;";
        public string DefaultSecretKey = "D1327sdfererCC7-df3#7DC4-41sdfsdA6-Bsdfsd8C6-CD25sdfdE2710920";
        public string DefaultIssuer = "www.artgallery.com";
        public string DefaultAudience = "www.artgallery.org";
        public string LogPath
        {
            get
            {
                return "/logs";
            }
        }
        public string DbConnectionString
        {
            get
            {
                return Environment.GetEnvironmentVariable("DB_CONNECTION_STRING") ?? DefaultString;
            }
        }


        public string SecretKey
        {
            get
            {
                return Environment.GetEnvironmentVariable("SECRET_KEY") ?? DefaultSecretKey;
            }
        }
        public TimeSpan TokenExpiryDuration
        {
            get => TimeSpan.FromDays(30);
        }
        public string Issuer
        {
            get
            {
                return Environment.GetEnvironmentVariable("ISSUER") ?? DefaultIssuer;
            }
        }
        public string Audience
        {
            get
            {
                return Environment.GetEnvironmentVariable("AUDIENCE") ?? DefaultAudience;
            }
        }
        public string ContentDirectory
        {
            get
            {

                string path = Path.Combine(Environment.CurrentDirectory, "Contents");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return path;
            }
        }
        public string AvatarDirectory
        {
            get
            {
                string path = Path.Combine(ContentDirectory, "Avatars");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return path;
            }
        }
    }
}
