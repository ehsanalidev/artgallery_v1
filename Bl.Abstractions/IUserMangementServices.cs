﻿using Bl.Abstractions.Dtos.Authenticate;
using DataAccessLAyer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bl.Abstractions
{
    public interface IUserMangementServices
    {
        Task<User> RegisterAsync(User user, String password);
        Task<AuthenticationResponse> AuthenticateAsync(string requestid, string password);
        Task<AuthenticationResponse> GenerateTokenAsync(User user, string SecretKey, string Issuer, string Audience, TimeSpan TokenExpiryDuration);

        Task CreateUserAvatarAsync(Guid userId, Stream avatar, string fileName);

    }
}
