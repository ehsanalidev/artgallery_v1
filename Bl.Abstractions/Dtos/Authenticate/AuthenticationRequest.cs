﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bl.Abstractions.Dtos.Authenticate
{
    public class AuthenticationRequest
    {

        [Required]
        [DefaultValue("passwprd")]
        public string Grant_Type { get; set; }
        [Required(ErrorMessage = "Username is requierd")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is requeird")]
        public string Password { get; set; }

    }
}
