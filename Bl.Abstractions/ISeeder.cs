﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bl.Abstractions
{
    public interface ISeeder
    {
        public void Seed();
    }
}
