﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLAyer.Enums
{
    public enum Gender
    {
        Male,
        Female,
        Diverse
    }
}
