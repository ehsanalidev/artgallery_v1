﻿using DataAccessLAyer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLAyer.Models
{
    public abstract class Entity<T>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public T Id { get; set; }
        [Column(TypeName= "nvarchar(50)")]
        public Status status { get; set; } = Status.Aktive;
        public DateTime CreationDate { get; set; } = DateTime.UtcNow;
    }
}
