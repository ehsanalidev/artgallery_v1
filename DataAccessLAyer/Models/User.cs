﻿using DataAccessLAyer.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DataAccessLAyer.Models;

public partial class User : Entity<Guid>
{
    

    public required string Username { get; set; } = null!;

    public required string? Email { get; set; }

    public required string? FirstName { get; set; }

    public required string? LastName { get; set; }

    public string? NationalCode { get; set; }

    public Gender Gender { get; set; }

    public DateTime LastVisit { get; set; }

    public string? Avatar { get; set; }

    public required string PasswordHash { get; set; } 

    public  required string PasswordSalt { get; set; }

    public DateTime? PasswordExpiryDate { get; set; }

    public string MobileNumber { get; set; } 

    public string? Address { get; set; }

    public AuthenticateVersion AuthenticateVersion { get; set; } = AuthenticateVersion.V1;


    public virtual ICollection<UserToken> UserTokens { get; set; } = new List<UserToken>();

    public virtual ICollection<Role> Roles { get; set; } = new List<Role>();
}
