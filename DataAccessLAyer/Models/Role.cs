﻿using System;
using System.Collections.Generic;

namespace DataAccessLAyer.Models;

public partial class Role : Entity<Guid>
{

    public string? Description { get; set; }

    public string Title { get; set; }


    public virtual ICollection<AuthorityToken> AuthorityTokens { get; set; } = new List<AuthorityToken>();

    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
