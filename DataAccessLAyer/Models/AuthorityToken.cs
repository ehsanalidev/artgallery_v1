﻿using System;
using System.Collections.Generic;

namespace DataAccessLAyer.Models;

public partial class AuthorityToken : Entity<string>
{
    

    public string? Description { get; set; }

    public virtual ICollection<Role> Roles { get; set; } = new List<Role>();
}
