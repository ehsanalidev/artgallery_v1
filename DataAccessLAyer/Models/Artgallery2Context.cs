﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLAyer.Models;

public partial class Artgallery2Context : DbContext, IArtGalleryContext
{
    public Artgallery2Context()
    {
    }

    public Artgallery2Context(DbContextOptions<Artgallery2Context> options)
        : base(options)
    {
    }

    public virtual DbSet<AuthorityToken> AuthorityTokens { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<UserToken> UserTokens { get; set; }

    

    //    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
    //        => optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=artgallery2;Integrated Security=True;;TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AuthorityToken>(entity =>
        {
            entity.ToTable("AuthorityToken");

            entity.Property(e => e.Id).HasMaxLength(100);
            entity.Property(e => e.Description).HasMaxLength(100);

            entity.HasMany(d => d.Roles).WithMany(d => d.AuthorityTokens)
            .UsingEntity<AuthorityTokensRole>();
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AuthorityTokenGroup");

            entity.ToTable("Role");

            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Description).HasMaxLength(200);
            entity.Property(e => e.Title).HasMaxLength(100);

 
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("User");

            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Email).HasMaxLength(200);
            entity.Property(e => e.FirstName).HasMaxLength(200);
            entity.Property(e => e.LastName).HasMaxLength(200);
            entity.Property(e => e.LastVisit).HasColumnType("datetime");
            entity.Property(e => e.MobileNumber).HasMaxLength(50);
            entity.Property(e => e.NationalCode).HasMaxLength(100);
            entity.Property(e => e.PasswordExpiryDate).HasColumnType("datetime");
            entity.Property(e => e.PasswordHash)
                .HasMaxLength(1000);

            entity.Property(e => e.PasswordSalt)
                .HasMaxLength(1000);
          
            entity.Property(e => e.Username).HasMaxLength(200);
        });

        modelBuilder.Entity<UserToken>(entity =>
        {
            entity.ToTable("UserToken");

            entity.HasIndex(e => e.UserId, "IX_UserToken_UserId");

            entity.Property(e => e.Id).ValueGeneratedOnAdd();
         
            entity.Property(e => e.ExpireDate).HasColumnType("datetime");

            entity.HasOne(d => d.User).WithMany(p => p.UserTokens)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserToken_User");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
