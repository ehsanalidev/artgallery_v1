﻿using System;
using System.Collections.Generic;

namespace DataAccessLAyer.Models;

public class UserToken : Entity<Guid> 
{
   

    public  Guid UserId { get; set; }

    public string Token { get; set; } = null!;

    public DateTime? ExpireDate { get; set; }

    public virtual User User { get; set; } = null!;
}
