FROM  mcr.microsoft.com/dotnet/aspnet:7.0 As base
#RUN apt-get update && apt install iputils-ping  && apt install net-tools -y
WORKDIR /app
#ENV APPNAME=mygallery
#ARG nugetUrl
EXPOSE 80
EXPOSE 443
EXPOSE 5000

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
