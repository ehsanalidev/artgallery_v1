﻿using Bl.Abstractions;
using DataAccessLAyer;
using DataAccessLAyer.Enums;
using DataAccessLAyer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace BussinessLayer.Services
{
    public class Seeder : ISeeder
    {
        public Seeder(IArtGalleryContext ArtGalleryContext)
        {
            this.ArtGalleryContext = ArtGalleryContext;
        }

        public IArtGalleryContext ArtGalleryContext { get; }

        public void Seed()
        {
            var authorityTokens = DefaultValues.AuthorityTokens
                 .GetToken()
                 .Select(x => x.Value)
                 .SelectMany(x => x)
                 .Select(x =>
                 {
                    if(!ArtGalleryContext.AuthorityTokens.Any(u=> u.Id== x)) 
                     {
                         ArtGalleryContext.AuthorityTokens.Add(

                             new AuthorityToken()
                             {
                                 Id = x,
                                 Description = x,
                                 status = Status.Aktive,
                             });
                     
                     }
                     return x;
                 }).ToList();

            ArtGalleryContext.SaveChanges();


            var roles = DefaultValues.DefaultRoles.Roles.Select(u =>
            {
                if (!ArtGalleryContext.Roles.Any(r => r.Id == u.Id))
                {
                    var Tokens = ArtGalleryContext.AuthorityTokens.ToList()
                    .Where(x => u.AuthorityTokens.Any(r => r.Id == x.Id)).ToList();
                    u.AuthorityTokens = Tokens;
                    ArtGalleryContext.Roles.Add(u);

                }
                else
                {

                    var role = ArtGalleryContext
                   .Roles
                   .Include(x => x.AuthorityTokens)
                   .FirstOrDefault(x => x.Id == u.Id);
                    var roleTokens = ArtGalleryContext.AuthorityTokens.ToList()
                     .Where(t => u.AuthorityTokens.Any(r => t.Id == t.Id)).ToList();
                    role.AuthorityTokens.Clear();
                    role.AuthorityTokens = roleTokens;

                }
                return u;
            }).ToList();

            ArtGalleryContext.SaveChanges();

             DefaultValues.DefaultUser.Users.Select(u =>
            {
                if (ArtGalleryContext.Users.Any(x => x.Id == u.Id))
                {
                    var user = ArtGalleryContext.Users.Include(e => e.Roles).FirstOrDefault(r => r.Id == u.Id);
                    var roles = ArtGalleryContext.Roles.Where(r => u.Roles.Any(t => r.Id == r.Id)).ToList();
                    user.Roles = roles;
                }
                else
                {
                    var roles = ArtGalleryContext.Roles.ToList().Where(r => u.Roles.Any(t => r.Id == r.Id)).ToList();
                    u.Roles = roles;
                    ArtGalleryContext.Users.Add(u);
                }

                return u;
            }).ToList();


            ArtGalleryContext.SaveChanges();

        }
    }
}
