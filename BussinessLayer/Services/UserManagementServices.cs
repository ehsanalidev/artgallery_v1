﻿using Bl.Abstractions;
using BussinessLayer.ArtGalleryExceptins;
using BussinessLayer.Utilietis;
using DataAccessLAyer;
using DataAccessLAyer.Enums;
using DataAccessLAyer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bl.Abstractions.Dtos;
using Bl.Abstractions.Dtos.Authenticate;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace BussinessLayer.Services
{
    public class UserManagementServices : IUserMangementServices
    {
        public UserManagementServices(IArtGalleryContext artGalleryContext, ISettings settings)
        {
            ArtGalleryContext = artGalleryContext;
            Settings = settings;
        }

        public IArtGalleryContext ArtGalleryContext { get; }
        public ISettings Settings { get; }

        public async Task<User> RegisterAsync(User user, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ArtGalleryException("Password is requered", ArtGalleryExceptionType.PasswordIsRequierd);
            if (ArtGalleryContext.Users.Where(u => u.Username == user.Username || u.Email == user.Email).FirstOrDefault() != null)
                throw new ArtGalleryException($"Username{user.Username} is already taken", ArtGalleryExceptionType.UserNameIsAlreadyTaken);
            user.PasswordHash = PasswordHelper.HashPassword(password, out string Salt);
            user.PasswordSalt = Salt;
            user.AuthenticateVersion = AuthenticateVersion.V1;
            user.CreationDate = DateTime.UtcNow;
            ArtGalleryContext.Users.Add(user);
            ArtGalleryContext.SaveChanges();
            return await Task.FromResult(user);

        }

        public async Task<AuthenticationResponse> AuthenticateAsync(string requestid, string password)
        {
            if (string.IsNullOrEmpty(requestid) || string.IsNullOrEmpty(password))
                throw new ArtGalleryException("Username or password is invalid", ArtGalleryExceptionType.UsernameOrPasswordIsInvalid);
            var user = ArtGalleryContext.Users.Where(u => u.Username == requestid || u.Email == requestid).FirstOrDefault();
            if (user == null)
                throw new ArtGalleryException("User not found!", ArtGalleryExceptionType.UserNotFound);
            if (!PasswordHelper.VerifyPassword(password, user.PasswordHash, user.PasswordSalt))
                throw new ArtGalleryException("Password or Username is invalid", ArtGalleryExceptionType.UsernameOrPasswordIsInvalid);
            return await GenerateTokenAsync(user, Settings.SecretKey, Settings.Issuer, Settings.Audience, Settings.TokenExpiryDuration);
        }

        public async Task<AuthenticationResponse> GenerateTokenAsync(User user, string secretKey, string issuer, string audience, TimeSpan tokenExpiryDuration)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secretKey);
            var authtokens = ArtGalleryContext
                .Users
                .Include(u => u.Roles)
                .ThenInclude(u => u.AuthorityTokens)
                .Where(u => u.Id == user.Id)
                .SelectMany(u => u.Roles).SelectMany(u => u.AuthorityTokens)
                .Select(u => u.Id).Distinct().ToList();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),

                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Email, user.Email),
                }),
                Issuer = issuer,
                Audience = audience,
                Expires = DateTime.UtcNow.Add(tokenExpiryDuration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            tokenDescriptor.Subject.AddClaims(authtokens.Select(u => new Claim(ClaimTypes.Role, u)).ToList());
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            ArtGalleryContext.UserTokens.Add(new UserToken
            {
                ExpireDate = tokenDescriptor.Expires.Value,
                Token = tokenString,
                User = user,
                CreationDate = DateTime.UtcNow,
                status = Status.Aktive,
                UserId = user.Id,
            });
            ArtGalleryContext.SaveChanges();
            AuthenticationResponse authenticateOut = new AuthenticationResponse
            {
                AccessToken = tokenString,
                ExpiresIn = (long)tokenDescriptor.Expires.Value.Subtract(DateTime.UtcNow).TotalSeconds,
                TokenType = "Bearer",
            };
            return await Task.FromResult(authenticateOut);
        }

        public  async Task CreateUserAvatarAsync(Guid userId, Stream avatar, string fileName)
        {
            if (userId == Guid.Empty)
                throw new ArgumentException("UserId is invalid");
            var user = ArtGalleryContext.Users.Where(u => u.Id == userId).FirstOrDefault();
            if (user == null)
                throw new ArgumentException("User not Found");
            FileInfo fileInfo= new FileInfo(fileName);
            user.Avatar = $"{Guid.NewGuid().ToString()}{fileInfo.Extension}";
            string avatarPath= Path.Combine(Settings.AvatarDirectory, user.Avatar);
            using (Stream fileStream = new FileStream(avatarPath, FileMode.Create, FileAccess.Write)) 
            {
            avatar.CopyTo(fileStream);
            }
            ArtGalleryContext.SaveChanges();
        }
    }
}
