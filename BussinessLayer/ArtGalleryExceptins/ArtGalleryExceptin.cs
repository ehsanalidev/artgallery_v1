﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.ArtGalleryExceptins
{
    internal class ArtGalleryException: Exception
    {
        public ArtGalleryException(string Message, ArtGalleryExceptionType artGalleryExceptionType): base(Message)
        {
            artGalleryExceptionType = ArtGalleryExceptionType;
        }
        public ArtGalleryExceptionType ArtGalleryExceptionType { get; set; }
    }
    public enum ArtGalleryExceptionType

    {
        UsernameOrPasswordIsInvalid,
        UserNotFound,
        PasswordIsRequierd,
        UserNameIsAlreadyTaken
    }
}
