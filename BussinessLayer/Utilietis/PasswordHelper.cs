﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Utilietis
{
    public class PasswordHelper
    {
        const int KeySize = 64;
        const int Iterations = 35000;
        static HashAlgorithmName hashAlgoritm = HashAlgorithmName.SHA512;
        public static string HashPassword(string Password, out string Salt)
        {
            if (Password == null) { throw new ArgumentException(Password); }
            if(string.IsNullOrWhiteSpace(Password)) { throw new ArgumentException("Value Cannot be empty or whiteSpace only string", "Password"); }
            var innerSalt = RandomNumberGenerator.GetBytes(KeySize);
            var hash = Rfc2898DeriveBytes.Pbkdf2(
                Encoding.UTF8.GetBytes(Password),
                innerSalt,
                Iterations,
                hashAlgoritm,
                KeySize
                );
            Salt = Convert.ToHexString(innerSalt);
            return Convert.ToHexString(hash);

            
        }
        public static bool VerifyPassword(string password, string hash, string salt)
        {

            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (hash.Length != 128) throw new ArgumentException("Invalid length of password hash (128 bytes expected).", "passwordHash");
            if (salt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");
            var saltArr = Convert.FromHexString(salt);
            var hashToCompare = Rfc2898DeriveBytes.Pbkdf2(password, saltArr, Iterations, hashAlgoritm, KeySize);
            return hashToCompare.SequenceEqual(Convert.FromHexString(hash));
        }
    }
}
