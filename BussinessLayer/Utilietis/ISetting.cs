﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Utilietis
{
    public interface ISettings
    {
        public string LogPath { get; }
        public string DbConnectionString { get; }
        public string SecretKey { get; }
        public TimeSpan TokenExpiryDuration { get; }
        public string Issuer { get; }
        public string Audience { get; }
        public string ContentDirectory { get; }
        public string AvatarDirectory { get; }
    }
}
