﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLAyer.Models;

namespace BussinessLayer.DefaultValues
{
    public static class DefaultRoles
    {
        public const string SysAdmin = "System Admin";

        public static List<Role> Roles
        { get
            {
                return new List<Role>()
                {
                    new Role()
                    {
                        Id= Guid.Parse("DF063611-52CB-4495-9251-37CBCAA0490C"),
                        Title=SysAdmin,
                        AuthorityTokens= AuthorityTokens.GetToken().SelectMany(u=> u.Value).Distinct().Select(u=> new AuthorityToken(){Id=u}).ToList(),
                    }
                };

            }
        }
    }
}
