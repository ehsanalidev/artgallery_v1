﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.DefaultValues
{
    public static class AuthorityTokens
    {
        public static class Users
        {
            public const string register = "Users/register";
            public const string profile = "Users/profile";
            public const string Avatar = "Users/avatar";
            public const string Signature = "Users/Signature";
        }

        public static class Authority
        {
            public const string GetAll = "authority/getall";
            public const string GetById = "authority/getbyid";
            public const string Create = "authority/create";
            public const string Update = "authority/update";
            public const string Delete = "authority/delete";
            public const string GetAuthorityTokens = "authority/getauthoritytokens";
            public const string GetAuthortiyGroupTokens = "authority/getauthortiygrouptokens";
            public const string AddToAuthorityGroup = "authority/addtoauthoritygroup";
            public const string RemoveFromAuthorityGroup = "authority/removefromauthoritygroup";
        }

        public static Dictionary<string, List<string>> GetToken()
        {
            var resault = typeof(AuthorityTokens)

                .GetNestedTypes()
                .Select(t => new
                {
                    Name = t.Name,
                    Fields = t.GetFields()
                    .Select(z => ((string)z.GetValue(null)) ?? "")
                    .ToList()

                }).ToDictionary(t=> t.Name, t=> t.Fields);
            return resault;
                
        }
    }
}
