﻿using BussinessLayer.Utilietis;
using DataAccessLAyer.Enums;
using DataAccessLAyer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.DefaultValues
{
    public class DefaultUser
    {
        public static List<User> Users
        {
            get
            {
                return new List<User>
                {
                    new User()
                    {
                        Id=Guid.Parse("DF063611-52CB-4495-9251-37CBCAA0490C"),
                        CreationDate = DateTime.UtcNow,
                        FirstName="Admin",
                        LastName="Admin",
                        status=Status.Aktive,
                        PasswordExpiryDate=null,
                        Username="Admin",
                        MobileNumber="09",
                        Email="admin@admin.com",
                        PasswordHash=PasswordHelper.HashPassword("Epo$123456#ldj*",out string salt),
                        NationalCode="11111",
                        PasswordSalt=salt,
                        Roles= DefaultRoles.Roles,
                        Gender=Gender.Male,
                        LastVisit=DateTime.UtcNow,
                    }
                };
            }
        }
    }
}
